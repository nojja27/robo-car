package noemipeter.nemethgulyas.nik.uniobuda.hu.robo_car


import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import noemipeter.nemethgulyas.nik.uniobuda.hu.robo_car.models.JoyStickClass
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.AsyncTask
import android.os.Handler
import android.util.Log
import android.widget.Toast
import noemipeter.nemethgulyas.nik.uniobuda.hu.robo_car.controllers.BluetoothSelectorController
import java.io.IOException
import java.util.*


class MainActivity : AppCompatActivity(), SensorEventListener {

    lateinit var mSensorManager : SensorManager
    var _onOffSwitch: Boolean = true
    var toSend: String = ""

    companion object {
        var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        var m_bluetoothSocket: BluetoothSocket? = null
        var m_isConnected: Boolean = false
        val random = Random()
        lateinit var m_bluetoothAdapter: BluetoothAdapter
        lateinit var m_address: String
        lateinit var m_progress: ProgressDialog
        lateinit var layoutMain: RelativeLayout
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int){

    }

    override fun onSensorChanged(event: SensorEvent?){
        var _x: Int = (event!!.values[0] * 25).toInt()
        var _y: Int = (event!!.values[1] * 25).toInt()

        if(_x > 100)  _x = 100
        if(_x < -100) _x = -100

        if(_y > 100)  _y = 100
        if(_y < -100) _y = -100

        toSend = "${_x+200}${_y+200}"

        if(!_onOffSwitch)
        {
            textView1.text = "X = ${_x}"
            textView2.text = "Y = ${_y}"
            textView3.text = "D : ${_x} : ${_y}"

            var _distance: Int = receiveCommand()

            if (_distance > 0 && _distance<10) layoutMain.setBackgroundColor(Color.rgb(255,0,0))
            if (_distance > 11 && _distance<20) layoutMain.setBackgroundColor(Color.rgb(205,0,0))
            if (_distance > 21 && _distance<30) layoutMain.setBackgroundColor(Color.rgb(155,0,0))
            if (_distance > 31 && _distance<40) layoutMain.setBackgroundColor(Color.rgb(105,0,0))
            if (_distance > 41 && _distance<50) layoutMain.setBackgroundColor(Color.rgb(55,0,0))
            if (_distance > 50 ) layoutMain.setBackgroundColor(Color.rgb(0,0,0))

            textView4.text = "T : ${_distance}"

            sendCommand(toSend)
        }
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        mSensorManager.registerListener(
                this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL
        )

        var textView1: TextView = findViewById(R.id.textView1) as TextView
        var textView2: TextView = findViewById(R.id.textView2) as TextView
        var textView3: TextView = findViewById(R.id.textView3) as TextView
        var textView4: TextView = findViewById(R.id.textView4) as TextView
        var layoutJoystick: RelativeLayout = findViewById(R.id.layout_joystick) as RelativeLayout
        layoutMain = findViewById(R.id.layout_main) as RelativeLayout

        val js = JoyStickClass(baseContext, layoutJoystick, R.drawable.image_button)
        js.setStickSize(150, 150)
        js.setLayoutSize(500, 500)
        js.setLayoutAlpha(150)
        js.setStickAlpha(100)
        js.setOffset(90)
        js.setMinimumDistance(50)

        layoutJoystick.setOnTouchListener { arg0, arg1 ->
            js.drawStick(arg1)
            if (arg1.action == MotionEvent.ACTION_DOWN || arg1.action == MotionEvent.ACTION_MOVE) {
                var _x = js.getX()
                var _y = js.getY()

                if(_x > 100)  _x = 100
                if(_x < -100) _x = -100

                if(_y > 100)  _y = 100
                if(_y < -100) _y = -100

                toSend = "${_x+200}${_y+200}"

                if(_onOffSwitch)
                {
                    textView1.text = "X : " + _x
                    textView2.text = "Y : " + _y
                    textView3.text = "D : ${_x} : ${_y}"

                    var _distance: Int = receiveCommand()

                    if (_distance > 0 && _distance<10) layoutMain.setBackgroundColor(Color.rgb(255,0,0))
                    if (_distance > 11 && _distance<20) layoutMain.setBackgroundColor(Color.rgb(205,0,0))
                    if (_distance > 21 && _distance<30) layoutMain.setBackgroundColor(Color.rgb(155,0,0))
                    if (_distance > 31 && _distance<40) layoutMain.setBackgroundColor(Color.rgb(105,0,0))
                    if (_distance > 41 && _distance<50) layoutMain.setBackgroundColor(Color.rgb(55,0,0))
                    if (_distance > 50 ) layoutMain.setBackgroundColor(Color.rgb(0,0,0))

                    textView4.text = "T : ${_distance}"

                    sendCommand(toSend)
                }

            }
            true
        }

        scanButton.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View): Unit {
                val intent = Intent(this@MainActivity,BluetoothSelectorController::class.java)
                startActivityForResult(intent,1)
            }
        })

        onButton.setOnClickListener(object: View.OnClickListener {
            override fun onClick(view: View): Unit {
                if(_onOffSwitch)
                {
                    _onOffSwitch = false
                    onButton.text = "Off"
                }else{
                    _onOffSwitch = true
                    onButton.text = "On"
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            m_address = data?.getStringExtra(BluetoothSelectorController.EXTRA_ADDRESS)!!
        }

        if (m_address != ""){
            ConnectToDevice(this).execute()
        }
    }

    private class ConnectToDevice(c: Context) : AsyncTask<Void, Void, String>() {
        private var connectSuccess: Boolean = true
        private val context: Context

        init {
            this.context = c
        }

        override fun onPreExecute() {
            super.onPreExecute()
            m_progress = ProgressDialog.show(context, "Connecting...", "please wait")
        }

        override fun doInBackground(vararg p0: Void?): String? {
            try {
                if (m_bluetoothSocket == null || !m_isConnected) {
                    m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    val device: BluetoothDevice = m_bluetoothAdapter.getRemoteDevice(m_address)
                    m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(m_myUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    m_bluetoothSocket!!.connect()


                }
            } catch (e: IOException) {
                connectSuccess = false
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (!connectSuccess) {
                Log.i("data", "couldn't connect")
            } else {
                m_isConnected = true
            }

            m_progress.dismiss()
        }
    }

    fun sendCommand(input: String) {
        if (m_bluetoothSocket != null) {
            try{
                var stx:Int = 0x02
                var etx:Int = 0x03
                m_bluetoothSocket!!.outputStream.write(stx)
                Thread.sleep(1)
                for(s in input){
                    m_bluetoothSocket!!.outputStream.write(s.toInt())
                    Thread.sleep(1)
                }
                m_bluetoothSocket!!.outputStream.write(etx)
            } catch(e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun receiveCommand() : Int{
        if (m_bluetoothSocket != null){
            var buffer = byteArrayOf()
            var bytes: Int
            try {
                bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                return bytes.toInt() - 48
            }catch (e: IOException)
            {
                e.printStackTrace()
            }
        }

        return 111111111
    }

    private fun disconnect() {
        if (m_bluetoothSocket != null) {
            try {
                m_bluetoothSocket!!.close()
                m_bluetoothSocket = null
                m_isConnected = false
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    override fun onResume() {
        super.onResume()

    }
}

