package noemipeter.nemethgulyas.nik.uniobuda.hu.robo_car.models

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams

public class JoyStickClass(private val mContext: Context, private val mLayout: ViewGroup, stick_res_id:Int) {

    private var STICK_ALPHA = 200
    private var LAYOUT_ALPHA = 200
    private var OFFSET = 0

    var params: ViewGroup.LayoutParams

    private var stick_width: Int = 0
    private var stick_height:Int = 0

    private var position_x = 0
    private var position_y = 0
    private var min_distance = 0
    private var distance = 0f
    private var angle = 0f

    private var draw: DrawCanvas
    var paint: Paint
    private var stick: Bitmap? = null

    private var touch_state = false

    init {
        stick = BitmapFactory.decodeResource(mContext.resources,
                stick_res_id)

        stick_width = stick!!.width
        stick_height = stick!!.height

        draw = DrawCanvas(mContext)
        paint = Paint()
        params = mLayout!!.getLayoutParams()
    }

    fun drawStick(arg1: MotionEvent) {
        position_x = (arg1.x - params.width / 2).toInt()
        position_y = (arg1.y - params.height / 2).toInt()
        distance = Math.sqrt(Math.pow(position_x.toDouble(), 2.0) + Math.pow(position_y.toDouble(), 2.0)).toFloat()
        angle = cal_angle(position_x.toFloat(), position_y.toFloat()).toFloat()


        if (arg1.action == MotionEvent.ACTION_DOWN) {
            if (distance <= params.width / 2 - OFFSET) {
                draw.position(arg1.x, arg1.y)
                draw()
                touch_state = true
            }
        } else if (arg1.action == MotionEvent.ACTION_MOVE && touch_state) {
            if (distance <= params.width / 2 - OFFSET) {
                draw.position(arg1.x, arg1.y)
                draw()
            } else if (distance > params.width / 2 - OFFSET) {
                var x = (Math.cos(Math.toRadians(cal_angle(position_x.toFloat(), position_y.toFloat()))) * (params.width / 2 - OFFSET)).toFloat()
                var y = (Math.sin(Math.toRadians(cal_angle(position_x.toFloat(), position_y.toFloat()))) * (params.height / 2 - OFFSET)).toFloat()
                x += (params.width / 2).toFloat()
                y += (params.height / 2).toFloat()
                draw.position(x, y)
                draw()
            } else {
                mLayout.removeView(draw)
            }
        } else if (arg1.action == MotionEvent.ACTION_UP) {
            mLayout.removeView(draw)
            touch_state = false
        }
    }

    fun getX(): Int {
        return if (distance > min_distance && touch_state) {
            position_x
        } else 0
    }

    fun getY(): Int {
        return if (distance > min_distance && touch_state) {
            position_y
        } else 0
    }

    fun setMinimumDistance(minDistance: Int) {
        min_distance = minDistance
    }

    fun setOffset(offset: Int) {
        OFFSET = offset
    }

    fun setStickAlpha(alpha: Int) {
        STICK_ALPHA = alpha
        paint.alpha = alpha
    }

    fun setLayoutAlpha(alpha: Int) {
        LAYOUT_ALPHA = alpha
        mLayout.getBackground().setAlpha(alpha)
    }

    fun setStickSize(width: Int, height: Int) {
        stick = Bitmap.createScaledBitmap(stick, width, height, false)
        stick_width = stick!!.width
        stick_height = stick!!.height
    }

    fun setLayoutSize(width: Int, height: Int) {
        params.width = width
        params.height = height
    }

    private fun cal_angle(x: Float, y: Float): Double {
        if (x >= 0 && y >= 0)
            return Math.toDegrees(Math.atan((y / x).toDouble()))
        else if (x < 0 && y >= 0)
            return Math.toDegrees(Math.atan((y / x).toDouble())) + 180
        else if (x < 0 && y < 0)
            return Math.toDegrees(Math.atan((y / x).toDouble())) + 180
        else if (x >= 0 && y < 0)
            return Math.toDegrees(Math.atan((y / x).toDouble())) + 360
        return 0.0
    }

    private fun draw() {
        try {
            mLayout.removeView(draw)
        } catch (e: Exception) {
        }

        mLayout.addView(draw)
    }

    private inner class DrawCanvas internal constructor(mContext: Context) : View(mContext) {
        internal var x: Float = 0.toFloat()
        internal var y: Float = 0.toFloat()

        public override fun onDraw(canvas: Canvas) {
            canvas.drawBitmap(stick, x, y, paint)
        }

        internal fun position(pos_x: Float, pos_y: Float) {
            x = pos_x - stick_width / 2
            y = pos_y - stick_height / 2
        }
    }
}